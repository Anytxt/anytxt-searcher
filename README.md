# AnyTXT Searcher

------

AnyTXT Searcher is a powerful local data full-text search engine, just like a local disk Google search engine. It is your free Google Desktop Search alternative.
AnyTXT Searcher has a powerful document parsing engine built in, which extracts the text of commonly used documents without installing any other software, and combines the built-in high-speed indexing system to store the metadata of the text. You can quickly find any text that exists on your computer with the AnyTXT Searcher. It works on Windows 10, 8, 7, Vista, XP, 2008, 2012, 2016 ...

### [Download Installer](https://anytxt.net/download/)
### [More ... ](https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=57&ved=2ahUKEwiqx4jT-JvmAhWDJjQIHUJhB6Y4MhAWMAZ6BAgIEAE&url=https%3A%2F%2Fanytxt.net%2F&usg=AOvVaw22wtPNrBgzzwvh2hRvZm9I)

### Formats Supported
> * Plain Text Format (txt, cpp, html etc.)
> * Microsoft Outlook (eml)
> * Microsoft Word (doc, docx)
> * Microsoft Excel (xls, xlsx)
> * Microsoft PowerPoint (ppt, pptx)
> * Portable Document Format (pdf) (beta)
> * eBook Format (epub, mobi, chm, fb2 etc.)(beta)
> * More Document Types are coming

### More Features
> * Microsoft Office (doc, xls, ppt) Supported
> * Microsoft Office 2007 (docx, xlsx, pptx, docm, xlsm, docm) Supported
> * PDF Supported(Beta)
> * Non-English document Supported
> * Full Text Search
> * Real Time Search (Beta)
> * SSD Optimization
> * Fast Index
> * Fast Search

### Release Log
#### 2020-6-13
 1. Added support for e-book format epub ;
 2. Added support for e-book format mobi;
 3. Added support for format chm;
 4. Added support for e-book format fb2;
 5. Added real time displaying the index status;
 6. Added support for High DPI;

#### 2020-4-12
 1. Added support for updating the index database manually;
 2. Added support for setting automatic update index cycle;
 3. Added support for starting and stopping indexing service;
 
#### 2020-2-27
 1. Added the command line;
 2. Fixed known issues;
 
#### 2019-11-29
 1. Try to fix 100% CPU usage issue;
 2. Fix the problem of re-indexing during file update;
 
#### 2019-11-9
 1. Added fuzzy matching search;
 2. Added whole matching search(Beta);
 3. Added Multi-language support. Currently, the Chinese language has been added due to there are many Chinese users;
 You are welcome to translate it(English.ini) into your local language, I will integrate it into the installation package;
 4. Fix some bugs;
 
#### 2019-10-4
 1. Added removal of the index;
 2. Added the index rule feature;
 3. Added the Ctrl+C feature;
 4. Added the Ctrl+X feature;
 5. Added the Delete feature;
 6. Added the automatic detection of the new version feature;
 7. Fix some bugs;
 
#### 2019-6-24
 1. Added snippets to the search results;
 
#### 2019-6-11
 1. Added an icon to the search button;
 
#### 2019-6-8
 1. Added support for the none-NTFS file system;
 2. Speed up file traversal;
 3. Reduced computer resource consumption;
 4. Fixed some bugs;
 
#### 2019-6-2
 1. Added a community link to help menu to get users request;
 2. Added support dragging for the search results list;
 
#### 2019-5-23:
 1. Optimized indexing speed;
 2. Optimized support for Arabic language based on user feedback;
 3. Optimized support for Chinese language based on user feedback;
 4. Optimized support for Korean language based on user feedback;
 5. Optimized support for Japanese language based on user feedback;
 6. Optimized the loading interface when the program starts;